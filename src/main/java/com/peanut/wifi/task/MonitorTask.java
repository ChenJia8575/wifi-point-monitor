package com.peanut.wifi.task;

import com.peanut.wifi.model.Alert;
import com.peanut.wifi.model.Result;
import com.peanut.wifi.util.Constant;
import com.peanut.wifi.util.JsonUtil;
import com.peanut.wifi.util.config.CompositeFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import static com.peanut.wifi.util.Constant.ALERT_MAX_CNT;
import static com.peanut.wifi.util.Constant.ALERT_MOBILE;

/**
 * desc:    定时任务处理数据,是否有堆积检查和报警
 * author:  llc
 * date:    2017-02-24 15:29
 */
public class MonitorTask implements Runnable {

    private static Logger logger = Logger.getLogger(MonitorTask.class);
    private boolean problem=false;
    private String hostAddress;
    @Override
    public void run() {
        InetAddress address = null;//获取的是本地的IP地址
        try {
            address = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        this.hostAddress = address.getHostAddress();
        System.out.println(hostAddress);
        while (true) {
            try {
                //logger.info("data-back检查定时任务数据堆积开始...");
                doTask();
                //logger.info("data-back检查定时任务数据堆积结束...");
                Thread.sleep(1000 * 20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void doTask() {
        try{
            // 开始报警的数据堆积上线
            int maxCnt = Integer.parseInt(CompositeFactory.getString(ALERT_MAX_CNT));
            // 报警通知的用户
            String mobiles = CompositeFactory.getString(ALERT_MOBILE);
            String alertMsg = "";
            Alert alert = new Alert();
            alert.setMobile(mobiles);
            alert.setSender("dataBack");
            // 检查消费积分奖励taskReportTopic任务数据堆积
            File dir = new File("/data/logs/wifi-user/bak");
            //File dir = new File("D://data/logs/wifi-user/bak");
            File[] files = dir.listFiles();
            if(files.length>=maxCnt){
                if(problem==false) {
                    alertMsg = hostAddress+"服务器的data-back后台任务，消费taskReportTopic时有数据堆积,堆积日志文件数:" + files.length;
                    logger.info(alertMsg);
                    alert.setAlert(alertMsg);
                    sendMsg(alert);
                    problem=true;
                }
            }else{
                if(problem) {
                    alertMsg = hostAddress+"服务器的data-back后台任务taskReportTopic数据堆积问题现已被修复。";
                    logger.info(alertMsg);
                    alert.setAlert(alertMsg);
                    sendMsg(alert);
                    problem=false;
                }
            }
        }catch (Exception e){
            logger.info(e);
            e.printStackTrace();
        }
    }

    /**
     * 发送报警短信
     *
     * @param alert
     */
    private void sendMsg(Alert alert) {
        final String alert_url = "http://106.14.25.116:1818/alert";
        try {
            RequestConfig config = RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(3000).build();
            CloseableHttpClient httpClient = HttpClientBuilder.create().setDefaultRequestConfig(config).build();
            CloseableHttpResponse response = null;
            HttpPost httpPost = new HttpPost(alert_url);
            String jsonReq = JsonUtil.toJson(alert);
            if (!StringUtils.isEmpty(jsonReq)) {
                httpPost.setEntity(new StringEntity(jsonReq, "UTF-8"));
            }
            response = httpClient.execute(httpPost);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                logger.error("status code is " + statusCode);
            } else {
                HttpEntity entity = response.getEntity();
                String rs = null;
                if (entity != null) {
                    rs = EntityUtils.toString(entity, "utf-8");
                }
                EntityUtils.consume(entity);
                Result resp = (Result) JsonUtil.fromJson(rs, Result.class);
                if (resp.getResultCode() != Constant.DEAL_SUCCESS) {
                    logger.error("result is " + rs);
                } else {
                    logger.info("[" + alert.getMobile() + "] alert success , alert is " + alert.getAlert());
                }
            }
        } catch (IOException e) {
            logger.error("发送报警短信出错", e);
        }
    }
}
