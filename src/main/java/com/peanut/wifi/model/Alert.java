package com.peanut.wifi.model;

/**
 * desc:
 * author:  llc
 * date:    2017-03-17 15:48
 */
public class Alert {
    // 告警信息
    private String alert;

    // 手机号 多个用,隔开
    private String mobile;

    // 发送端 monitor agent
    private String sender;

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
