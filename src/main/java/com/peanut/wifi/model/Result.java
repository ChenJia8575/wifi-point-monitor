package com.peanut.wifi.model;

/**
 * Created by lenovo on 2017/2/16.
 */
public class Result {

    private Integer resultCode;

    private String resultMsg;

    public Result(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }
}
