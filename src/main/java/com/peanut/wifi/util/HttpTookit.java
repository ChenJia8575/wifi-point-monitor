package com.peanut.wifi.util;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * desc:    Http工具类
 * author:  ljt
 * date:    2016-07-14 18:02
 */
public class HttpTookit {

    /**
     * 从请求体中读取客户端发送的JSON串
     * @param stream
     *            输入流
     * @return String 类型，接收到的JSON串
     */
    public static String readStringFromRequestBody(InputStream stream) {
        if(stream == null){
            return null;
        }
        StringBuffer sb = new StringBuffer();
        char[] buf = new char[1024];
        int len = -1;
        try {
            InputStreamReader reader = new InputStreamReader(stream, "UTF-8");
            while ((len = reader.read(buf)) != -1) {
                sb.append(new String(buf, 0, len));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(stream);
        }
        return sb.toString();
    }


    /**
     * 回写响应
     * @param json
     * @param response
     */
    public static void writeString(String json, HttpServletResponse response) {
        response.setContentType("application/json");
        response.setCharacterEncoding("utf-8");
        ServletOutputStream os = null;
        try {
            os = response.getOutputStream();
            os.write(json.getBytes("UTF-8"));
            os.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(os);
        }
    }

    /**
     * 回写响应
     * @param respByte
     * @param response
     */
	public static void writeByte(byte[] respByte, HttpServletResponse response) {
		response.setContentType("application/json");
		response.setCharacterEncoding("utf-8");
		ServletOutputStream os = null;
		try {
			os = response.getOutputStream();
			os.write(respByte);
			os.flush();
		} catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(os);
        }
	}
}
