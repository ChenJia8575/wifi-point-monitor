package com.peanut.wifi.util;

import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;

/**
 * desc:    Redis连接池
 * author:  llc
 * date:    2017-02-15 16:09
 */

public class JedisPoolUtils {

    private static JedisSentinelPool pool;

    /**
     * 初始化Redis连接池
     */
    public synchronized static void init(String host,String pwd){

        // 使用HashSet添加多个sentinel
        HashSet<String> sentinels = new HashSet<String>();
        // 添加sentinel主机和端口
        String[] groups = host.split("&");
        for (String single:groups) {
            sentinels.add(single);
        }

        if(null != pool){
            return;
        }
        // 建立连接池配置参数
        JedisPoolConfig config = new JedisPoolConfig();
        // 设置最大连接数
        config.setMaxTotal(7000);
        // 设置最大阻塞时间，毫秒
        config.setMaxWaitMillis(5000);
        // 设置空间连接
        config.setMaxIdle(10);

        // 通过Jedis连接池创建一个Sentinel连接池
        if(!StringUtils.isEmpty(pwd)) {
             pool = new JedisSentinelPool("master", sentinels, config, pwd);
        }else{
             pool = new JedisSentinelPool("master", sentinels,config);
        }
        // 获取master的主机和端口
        HostAndPort currentHostMaster = pool.getCurrentHostMaster();
        // 从Sentinel池中获取资源
        Jedis resource = pool.getResource();

        // 测试一下
        Jedis jedis = null;
        try {
            jedis = JedisPoolUtils.getJedis();
            jedis.ping();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            JedisPoolUtils.returnRes(jedis);
        }
    }

//    public static void main(String[] agrs){
//        init("120.234.51.108:6379","nfyg123");
//    }
    /**
     * 获取一个jedis 对象
     * @return
     */
    public static Jedis getJedis() {
        return pool.getResource();
    }

    /**
     * 归还一个连接
     * @param jedis
     */
    public static void returnRes(Jedis jedis) {
        if(null != jedis){
            pool.returnResource(jedis);
        }
    }
}
