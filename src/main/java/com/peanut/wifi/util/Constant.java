package com.peanut.wifi.util;

/**
 * desc:    常量类
 * author:  ljt
 * date:    2016-07-14 15:59
 */
public class Constant {

    /** 处理成功 **/
    public static final int DEAL_SUCCESS = 0;

    /** 处理失败 **/
    public static final int DEAL_FAIL = 1;

    /** 处理异常 **/
    public static final int DEAL_EXCEPTION = 2;

    /** 图片 **/
    public static final Integer IMAGE_FILE = 1;

    /** 视频 **/
    public static final Integer VIDEO_FILE = 2;

    /** 频道类型 1-资讯; 2-视屏 **/
    public static final Integer CHANNEL_TYPE_ARTICLE = 1;

    /** 频道类型 2-视屏 **/
    public static final Integer CHANNEL_TYPE_VIDEO = 2;

    /** 冒号分割 **/
    public static final String COLON = ":";

    /** 服务启动时初始化redis缓存开关 **/
    public static String INIT_REDIS_ISOPEN = "INIT_REDIS_ISOPEN";

    public static String REDIS_SERVER = "REDIS_SERVER";

    public static String REDIS_PWD = "REDIS_PWD";

    /** 展示方式:1-单图 **/
    public static final Integer SHOW_TYPE_SINGLE = 1;

    /** 展示方式:2-多图 **/
    public static final Integer SHOW_TYPE_PIC3 = 2;

    /** 展示方式:3-大通栏带标题 **/
    public static final Integer SHOW_TYPE_BIG_TITLE = 3;

    /** ftp地址 **/
    public static final String RESOURCE_IMAGE_URL = "RESOURCE_IMAGE_URL";

    /** 信息流类型:1-资讯 **/
    public static final int ARTICLE_TYPE_INFORMATION = 1;

    /** 信息流类型:2-视频; **/
    public static final int ARTICLE_TYPE_VIDEO = 2;

    /** 信息流类型:3-图集 **/
    public static final int ARTICLE_TYPE_ALBUM = 3;

    /** api地址 **/
    public static final String API_URL = "API_URL";

    /** 用户缓存key **/
    public static final String USER_INFO = "MOBILE_";

    /** 最大堆积数,超过这个数,则报警 **/
    public static final String ALERT_MAX_CNT = "ALERT_MAX_CNT";

    /** 需要报警的手机号,用","分割 **/
    public static final String ALERT_MOBILE = "ALERT_MOBILE";
}
