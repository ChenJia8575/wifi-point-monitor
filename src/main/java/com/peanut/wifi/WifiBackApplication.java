package com.peanut.wifi;

import com.peanut.wifi.util.config.CompositeFactory;
import com.peanut.wifi.task.MonitorTask;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.sql.DataSource;


@EnableAutoConfiguration
@SpringBootApplication
@EnableScheduling
@ComponentScan
public class WifiBackApplication {

    private static Logger logger = Logger.getLogger(WifiBackApplication.class);

    private static ApplicationContext applicationContext;

	public static void main(String[] args) {
        applicationContext = SpringApplication.run(WifiBackApplication.class, args);
        initConfig();
        initThread();
        logger.info("SpringBoot Start Success");

	}

    private static void initThread() {
        Thread monitor = new Thread(new MonitorTask());
        monitor.setDaemon(true);
        monitor.start();
    }


    private static void initConfig() {
        // 读取主配置文件  这里可以把properties和xml配置文件统一放在一个xml中，遍历加载
        try {
            PropertiesConfiguration propertiesConfiguration1 = new PropertiesConfiguration("resultCode.properties");
            PropertiesConfiguration propertiesConfiguration2 = new PropertiesConfiguration("systemConfig.properties");
            CompositeFactory.getInstance().addConfiguration(propertiesConfiguration1);
            CompositeFactory.getInstance().addConfiguration(propertiesConfiguration2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public static <T> T getBeanByType(Class clazz) throws BeansException {
        return (T) applicationContext.getBean(clazz);
    }

}

